package ppa.labs.demojpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ppa.labs.demojpa.bean.Address;
import ppa.labs.demojpa.bean.User;
import ppa.labs.demojpa.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
	private static final Logger log = LoggerFactory.getLogger(DemoJpaMain.class);
	@Autowired
	private UserRepository userRepository;
	
	@Test
	@Ignore
	public void createUserTest() {
		User user01	= mockUser("John", "DOE", mockAddresses());
		userRepository.save(user01);
	}
	
	@Test
	public void selectUserTest() {
		// TODO - 
	}
	
	private User mockUser (String firstName, String lastName, List<Address> lsAddresses) {
		User user	= new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setLsAddresses(lsAddresses);
		return user;
	}
	
	private Address mockAddress (String city, String zipCode, String streetName, Integer num) {
		Address addr	= new Address();
		addr.setCity(city);
		addr.setZipCode(zipCode);
		addr.setStreetName(streetName);
		addr.setNum(num);
		return addr;
	}
	
	private List<Address> mockAddresses () {
		List<Address> addresses	= new ArrayList<>();
		addresses.add(mockAddress("SAN FRANSISCO", "05566", "impasse de la serpette", 6));
		addresses.add(mockAddress("NEW YORK", "45678", "rue des ponts", 13));
		addresses.add(mockAddress("CATMANDOU", "88990", "rue du major Salvieri", 467));
		return addresses;
	}
}
