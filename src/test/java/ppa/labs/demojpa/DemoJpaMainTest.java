package ppa.labs.demojpa;

import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ppa.labs.demojpa.bean.Bill;
import ppa.labs.demojpa.bean.Customer;
import ppa.labs.demojpa.repository.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class DemoJpaMainTest {
	private static final Logger log = LoggerFactory.getLogger(DemoJpaMain.class);
	@Autowired
	private CustomerRepository customerRepository;

	@Test
	public void demo() {

		// save a few customers
		Bill bill					= new Bill(1000.00, "purchasing");
		Map<String, Bill> mapBill	= new HashMap<String, Bill>();
		Customer customer					= new Customer("Jack", "Bauer");
		bill.setCustomer(customer);
		mapBill.put(bill.getDomain(), bill);
		customer.setBillByDomain(mapBill);
		customerRepository.save(customer);
		
		bill					= new Bill(2000.00, "renting");
		mapBill	= new HashMap<String, Bill>();
		customer					= new Customer("Chloe", "O'Brian");
		bill.setCustomer(customer);
		mapBill.put(bill.getDomain(), bill);
		customer.setBillByDomain(mapBill);
		customerRepository.save(customer);
		
		bill					= new Bill(3000.00, "purchasing");
		mapBill	= new HashMap<String, Bill>();
		customer					= new Customer("Kim", "Bauer");
		bill.setCustomer(customer);
		mapBill.put(bill.getDomain(), bill);
		customer.setBillByDomain(mapBill);
		customerRepository.save(customer);
		
		bill					= new Bill(4000.00, "reting");
		mapBill	= new HashMap<String, Bill>();
		customer					= new Customer("David", "Palmer");
		bill.setCustomer(customer);
		mapBill.put(bill.getDomain(), bill);
		customer.setBillByDomain(mapBill);
		customerRepository.save(customer);
		
		bill					= new Bill(5000.00, "purchasing");
		mapBill	= new HashMap<String, Bill>();
		customer					= new Customer("Michelle", "Dessler");
		bill.setCustomer(customer);
		mapBill.put(bill.getDomain(), bill);
		customer.setBillByDomain(mapBill);
		customerRepository.save(customer);


		// fetch all customers
		log.info("Customers found with findAll():");
		log.info("-------------------------------");
		for (Customer customer1 : customerRepository.findAll()) {
			log.info(customer1.toString());
		}
		log.info("");

		// fetch an individual customer by ID
		Customer customer2 = customerRepository.findById(1L);
		log.info("Customer found with findById(1L):");
		log.info("--------------------------------");
		log.info(customer2.toString());
		log.info("");

		// fetch customers by last name
		log.info("Customer found with findByLastName('Bauer'):");
		log.info("--------------------------------------------");
		customerRepository.findByLastName("Bauer").forEach(bauer -> {
			log.info(bauer.toString());
		});
		log.info("");
		

	}
}
