package ppa.labs.demojpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ppa.labs.demojpa.bean.Customer;
import ppa.labs.demojpa.repository.CustomerRepository;

@Controller
@RequestMapping(path="/demo")
public class CustomerController {
	@Autowired
	private CustomerRepository customerRepository;

	@PostMapping(path="/add") // Map ONLY POST Requests
	public @ResponseBody String addNewCustomer (
			@RequestParam String firstName,
			@RequestParam String lastName) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request

		Customer customer = new Customer();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customerRepository.save(customer);
		return "Saved";
	}

	@GetMapping(path="/all")
	public @ResponseBody Iterable<Customer> getAllCustomers() {
		// This returns a JSON or XML with the users
		return customerRepository.findAll();
	}
}
