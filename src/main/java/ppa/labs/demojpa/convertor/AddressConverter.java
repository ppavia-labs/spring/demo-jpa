package ppa.labs.demojpa.convertor;

import java.io.IOException;
import java.util.List;

import javax.persistence.AttributeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import ppa.labs.demojpa.bean.Address;


public class AddressConverter implements AttributeConverter<List<Address>, String> {

	@Override
	public String convertToDatabaseColumn(List<Address> addresses) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(addresses);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Address> convertToEntityAttribute(String dbData) {
		ObjectMapper mapper 			= new ObjectMapper();
		TypeFactory typeFactory			= mapper.getTypeFactory();
		CollectionType collectionType	= typeFactory.constructCollectionType(List.class, Address.class);
		try {
			return mapper.readValue(dbData, collectionType);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
