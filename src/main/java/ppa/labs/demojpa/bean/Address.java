package ppa.labs.demojpa.bean;

public class Address {
	//@Id
	//private Integer id;
	
	//@Column(table = "ADDR_CITY", columnDefinition = "varchar(250)")
	private String city;
	
	//@Column(table = "ADDR_ZIP_CODE", columnDefinition = "varchar(5)")
	private String zipCode;
	
	//@Column(table = "ADDR_NAME_ROAD", columnDefinition = "varchar(250)")
	private String streetName;
	
	//@Column(table = "ADDR_NUM", columnDefinition = "int")
	private Integer num;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
}
