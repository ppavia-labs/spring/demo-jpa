package ppa.labs.demojpa.bean;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import ppa.labs.demojpa.convertor.AddressConverter;

@Entity(name = "USERS")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "USER_FIRSTNAME", columnDefinition = "varchar(250)")
	private String firstName;
	
	@Column(name = "USER_LASTNAME", columnDefinition = "varchar(250)")
	private String lastName;
	
	@Column(name = "USER_ADDRESSES", columnDefinition = "varchar(5000)")
	@Convert(converter = AddressConverter.class)
	private List<Address> lsAddresses;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Address> getLsAddresses() {
		return lsAddresses;
	}

	public void setLsAddresses(List<Address> lsAddresses) {
		this.lsAddresses = lsAddresses;
	}

	public Integer getId() {
		return id;
	}	
}
