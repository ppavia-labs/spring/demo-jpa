package ppa.labs.demojpa.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ppa.labs.demojpa.bean.User;

public interface UserRepository extends CrudRepository<User, Long> {
	List<User> findByLastName(String lastName);
	User findById(long id);
}
