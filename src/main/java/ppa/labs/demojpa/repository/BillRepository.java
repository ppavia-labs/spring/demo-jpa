package ppa.labs.demojpa.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ppa.labs.demojpa.bean.Bill;

public interface BillRepository extends CrudRepository<Bill, Long> {
	
	List<Bill> findByDomain(String domain);
	Bill findById(long id);
}
