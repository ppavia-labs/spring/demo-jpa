package ppa.labs.demojpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJpaMain {
	private static final Logger log = LoggerFactory.getLogger(DemoJpaMain.class);
	public static void main(String[] args) {
		SpringApplication.run(DemoJpaMain.class, args);
	}
}
