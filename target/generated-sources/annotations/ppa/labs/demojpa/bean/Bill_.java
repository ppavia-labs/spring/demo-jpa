package ppa.labs.demojpa.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bill.class)
public abstract class Bill_ {

	public static volatile SingularAttribute<Bill, Double> amount;
	public static volatile SingularAttribute<Bill, String> domain;
	public static volatile SingularAttribute<Bill, Long> id;
	public static volatile SingularAttribute<Bill, Customer> customer;

	public static final String AMOUNT = "amount";
	public static final String DOMAIN = "domain";
	public static final String ID = "id";
	public static final String CUSTOMER = "customer";

}

